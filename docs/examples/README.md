---
title: Examples
date: 2018-07-25 16:10:11
---

# Follow along as we create two amazing GNOME apps in GJS: Tags and TicTacToe

<ShowCase link="tags/" title="Tags" subtitle="Learn how to build a tags and notes app for your files!" image="" />
<ShowCase link="tictactoe/" title="TicTacToe" subtitle="Learn how to build a fun game!" image="" />
